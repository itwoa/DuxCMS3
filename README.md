# DuxCMS 3

采用模块化结构开发

独立的DuxPHP框架

基于大量第三方优秀JS框架封装的类库

前端使用UIKIT

二次开发的KindEditor更适合现代化使用

composer的支持方便二次开发

包含默认演示模板与安装演示数据

支持PHP7.0以上版本

# 说明

DuxCMS 3为Dux新系列产品的基础功能版本，使用与二次开发请遵循开源协议使用，目前趋于完善功能，为了您与开发者的身心健康，管理系统不兼容IE8等系列低级浏览器

# 环境

PHP5.6+

Mysql 5.6+

Apache/Nginx

现代化浏览器 Chrome 30+ IE11+ FireFox 22+

# 交流

QQ群: 131331864

为了您的问题能尽快解决,有bug请提交在issues中,切勿在Q群刷屏,提问时请带上您的运行环境以便问题能够快速解决!

# 伪静态

需要伪静态支持,规则如下

````
RewriteEngine On

RewriteCond %{REQUEST_FILENAME} !-f

RewriteCond %{REQUEST_FILENAME} !-d

RewriteRule ^(.*)$ index.php [QSA,L]

````