# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.38)
# Database: cms
# Generation Time: 2018-05-12 02:32:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table dux_article
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_article`;

CREATE TABLE `dux_article` (
  `article_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) NOT NULL DEFAULT '0' COMMENT '栏目ID',
  `content_id` int(10) NOT NULL DEFAULT '0' COMMENT '内容ID',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`article_id`),
  KEY `class_id` (`class_id`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_article_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_article_class`;

CREATE TABLE `dux_article_class` (
  `class_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL DEFAULT '0' COMMENT '上级栏目',
  `category_id` int(10) NOT NULL DEFAULT '0',
  `tpl_class` varchar(250) NOT NULL DEFAULT '' COMMENT '栏目模板',
  `tpl_content` varchar(250) NOT NULL DEFAULT '' COMMENT '内容模板',
  PRIMARY KEY (`class_id`),
  KEY `category_id` (`category_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_page
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_page`;

CREATE TABLE `dux_page` (
  `page_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL DEFAULT '0' COMMENT '栏目ID',
  `label` varchar(250) NOT NULL COMMENT '标签',
  `parent_id` int(10) NOT NULL DEFAULT '0' COMMENT '上级栏目',
  `tpl` varchar(250) NOT NULL DEFAULT '' COMMENT '页面模板',
  `content` text NOT NULL COMMENT '内容',
  PRIMARY KEY (`page_id`),
  KEY `parent_id` (`parent_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_adv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_adv`;

CREATE TABLE `dux_site_adv` (
  `adv_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pos_id` int(10) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `url_ext` text NOT NULL,
  `image` varchar(250) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `start_time` int(10) NOT NULL DEFAULT '0',
  `stop_time` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0',
  `view` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`adv_id`),
  KEY `pos_id` (`pos_id`),
  KEY `status` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_adv_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_adv_position`;

CREATE TABLE `dux_site_adv_position` (
  `pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(10) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '广告位标题图',
  `name` varchar(250) NOT NULL DEFAULT '',
  `label` varchar(50) NOT NULL,
  `number` int(3) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pos_id`),
  KEY `status` (`name`),
  KEY `type` (`label`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_site_adv_position` WRITE;
/*!40000 ALTER TABLE `dux_site_adv_position` DISABLE KEYS */;

INSERT INTO `dux_site_adv_position` (`pos_id`, `region_id`, `image`, `name`, `label`, `number`, `sort`, `type`, `status`)
VALUES
	(1,1,'','首页幻灯片','banner',5,0,1,1);

/*!40000 ALTER TABLE `dux_site_adv_position` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_site_adv_region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_adv_region`;

CREATE TABLE `dux_site_adv_region` (
  `region_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '区域名称',
  `label` varchar(50) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_site_adv_region` WRITE;
/*!40000 ALTER TABLE `dux_site_adv_region` DISABLE KEYS */;

INSERT INTO `dux_site_adv_region` (`region_id`, `name`, `label`, `type`, `status`)
VALUES
	(1,'首页广告','index',0,1);

/*!40000 ALTER TABLE `dux_site_adv_region` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_site_class
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_class`;

CREATE TABLE `dux_site_class` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) NOT NULL DEFAULT '0' COMMENT '模型ID',
  `name` varchar(250) NOT NULL DEFAULT '' COMMENT '名称',
  `subname` varchar(250) NOT NULL DEFAULT '' COMMENT '副名称',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `url` varchar(259) NOT NULL DEFAULT '' COMMENT '外部链接',
  `image` varchar(250) NOT NULL DEFAULT '' COMMENT '形象图',
  `keyword` varchar(250) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(250) NOT NULL DEFAULT '' COMMENT '描述',
  `filter_id` int(10) NOT NULL DEFAULT '0' COMMENT '筛选ID',
  PRIMARY KEY (`category_id`),
  KEY `model_id` (`model_id`),
  KEY `filter_id` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_config`;

CREATE TABLE `dux_site_config` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  `content` varchar(250) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_site_config` WRITE;
/*!40000 ALTER TABLE `dux_site_config` DISABLE KEYS */;

INSERT INTO `dux_site_config` (`config_id`, `name`, `content`, `description`)
VALUES
	(1,'tpl_index','index','默认首页模板'),
	(2,'tpl_class','list','默认栏目模板'),
	(3,'tpl_content','content','默认内容模板'),
	(6,'info_title','DuxCMS3','站点标题'),
	(7,'info_keyword','DuxCMS,DuxPHP,开源内容管理系统,免费CMS','站点关键词'),
	(8,'info_desc','DuxCMS3是一款简单易用的免费开源的内容管理系统','站点描述'),
	(9,'info_copyright','Copyright@2013-2018 duxcms.com  All Rights Reserved.','版权信息'),
	(10,'info_email','admin@duxcms.com','站点邮箱'),
	(11,'info_tel','','站点电话'),
	(12,'tpl_name','default','模板目录'),
	(13,'tpl_tags','tag','标签模板'),
	(15,'tpl_search','search','搜索模板'),
	(16,'info_name','DuxCMS','站点名称'),
	(17,'config_site_status','1','站点状态'),
	(18,'config_mobile_status','0','移动状态'),
	(19,'config_site_error','站定维护中，本次维护预计需要4个小时，请谅解。','关闭说明'),
	(20,'tpl_res','','');

/*!40000 ALTER TABLE `dux_site_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_site_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_content`;

CREATE TABLE `dux_site_content` (
  `content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(20) NOT NULL COMMENT '应用名',
  `pos_id` varchar(250) NOT NULL DEFAULT '0' COMMENT '推荐位',
  `title` varchar(250) NOT NULL DEFAULT '' COMMENT '标题',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `subtitle` varchar(250) NOT NULL DEFAULT '' COMMENT '副标题',
  `image` varchar(250) NOT NULL DEFAULT '' COMMENT '形象图',
  `url` varchar(250) NOT NULL DEFAULT '' COMMENT '外部链接',
  `keyword` varchar(250) NOT NULL DEFAULT '' COMMENT '关键词',
  `description` varchar(250) NOT NULL DEFAULT '' COMMENT '描述',
  `tpl` varchar(50) NOT NULL DEFAULT '' COMMENT '模板名',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` varchar(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `view` int(10) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `source` varchar(250) NOT NULL DEFAULT '' COMMENT '来源',
  `auth` varchar(250) NOT NULL DEFAULT '' COMMENT '作者',
  `editor` varchar(250) NOT NULL DEFAULT '' COMMENT '编辑',
  `tags_id` varchar(250) NOT NULL DEFAULT '' COMMENT 'tags',
  PRIMARY KEY (`content_id`),
  KEY `pos_id` (`pos_id`),
  KEY `title` (`title`),
  KEY `status` (`status`),
  KEY `sort` (`sort`),
  KEY `create_time` (`create_time`),
  KEY `view` (`view`),
  KEY `tags_id` (`tags_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_content_attr
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_content_attr`;

CREATE TABLE `dux_site_content_attr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` int(10) NOT NULL DEFAULT '0' COMMENT '内容ID',
  `attr_id` int(10) NOT NULL DEFAULT '0' COMMENT '属性ID',
  `value` varchar(250) NOT NULL DEFAULT '' COMMENT '属性值',
  PRIMARY KEY (`id`),
  KEY `content_id` (`content_id`),
  KEY `attr_id` (`attr_id`),
  KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_filter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_filter`;

CREATE TABLE `dux_site_filter` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_filter_attr
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_filter_attr`;

CREATE TABLE `dux_site_filter_attr` (
  `attr_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '属性ID',
  `filter_id` int(10) NOT NULL DEFAULT '0' COMMENT '筛选ID',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '控件类型',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `value` text NOT NULL COMMENT '属性值',
  `filter` tinyint(1) NOT NULL DEFAULT '0' COMMENT '筛选',
  PRIMARY KEY (`attr_id`),
  KEY `filter_id` (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_form
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_form`;

CREATE TABLE `dux_site_form` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '表单名称',
  `description` varchar(250) NOT NULL DEFAULT '' COMMENT '表单描述',
  `label` varchar(50) NOT NULL DEFAULT '' COMMENT '表单标识',
  `tpl_list` varchar(50) NOT NULL DEFAULT '' COMMENT '列表模板',
  `tpl_info` varchar(50) NOT NULL DEFAULT '' COMMENT '内容模板',
  `status_list` tinyint(1) NOT NULL DEFAULT '0',
  `status_info` tinyint(1) NOT NULL DEFAULT '0',
  `submit` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_form_field
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_form_field`;

CREATE TABLE `dux_site_form_field` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int(10) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '字段名称',
  `label` varchar(50) NOT NULL DEFAULT '' COMMENT '字段标识',
  `type` varchar(50) NOT NULL DEFAULT '' COMMENT '字段类型',
  `tip` varchar(250) NOT NULL DEFAULT '' COMMENT '字段提示',
  `must` tinyint(1) NOT NULL DEFAULT '0' COMMENT '必须字段',
  `default` varchar(250) NOT NULL DEFAULT '' COMMENT '默认值',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '字段顺序',
  `config` text NOT NULL COMMENT '字段配置',
  `show` tinyint(1) NOT NULL DEFAULT '0' COMMENT '列表显示',
  `submit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '前台提交',
  PRIMARY KEY (`field_id`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_fragment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_fragment`;

CREATE TABLE `dux_site_fragment` (
  `fragment_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL DEFAULT '' COMMENT '描述',
  `content` text NOT NULL COMMENT '内容',
  `editor` tinyint(1) NOT NULL DEFAULT '0' COMMENT '编辑器',
  PRIMARY KEY (`fragment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_model
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_model`;

CREATE TABLE `dux_site_model` (
  `model_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '模型名称',
  `description` varchar(250) NOT NULL DEFAULT '' COMMENT '模型描述',
  `label` varchar(50) NOT NULL DEFAULT '' COMMENT '模型标识',
  PRIMARY KEY (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_model_field
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_model_field`;

CREATE TABLE `dux_site_model_field` (
  `field_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '字段名称',
  `label` varchar(50) NOT NULL DEFAULT '' COMMENT '字段标识',
  `type` varchar(50) NOT NULL DEFAULT '' COMMENT '字段类型',
  `tip` varchar(250) NOT NULL DEFAULT '' COMMENT '字段提示',
  `must` tinyint(1) NOT NULL DEFAULT '0' COMMENT '必须字段',
  `default` varchar(250) NOT NULL DEFAULT '' COMMENT '默认值',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '字段顺序',
  `config` text NOT NULL COMMENT '字段配置',
  PRIMARY KEY (`field_id`),
  KEY `model_id` (`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_nav
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_nav`;

CREATE TABLE `dux_site_nav` (
  `nav_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL DEFAULT '0' COMMENT '上级ID',
  `group_id` int(10) NOT NULL DEFAULT '0' COMMENT '分组ID',
  `name` varchar(250) NOT NULL DEFAULT '' COMMENT '导航名称',
  `url` varchar(250) NOT NULL DEFAULT '' COMMENT '外链地址',
  `subname` varchar(10) NOT NULL DEFAULT '' COMMENT '导航副名称',
  `image` varchar(250) NOT NULL DEFAULT '' COMMENT '导航封面图',
  `keyword` varchar(250) NOT NULL DEFAULT '' COMMENT '导航关键词',
  `description` varchar(250) NOT NULL DEFAULT '' COMMENT '导航描述',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`nav_id`),
  KEY `parent_id` (`parent_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_nav_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_nav_group`;

CREATE TABLE `dux_site_nav_group` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '' COMMENT '分组名称',
  `description` varchar(250) NOT NULL DEFAULT '' COMMENT '分组描述',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_site_nav_group` WRITE;
/*!40000 ALTER TABLE `dux_site_nav_group` DISABLE KEYS */;

INSERT INTO `dux_site_nav_group` (`group_id`, `name`, `description`)
VALUES
	(1,'默认分组','系统默认导航');

/*!40000 ALTER TABLE `dux_site_nav_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_site_position
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_position`;

CREATE TABLE `dux_site_position` (
  `pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '',
  `sort` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pos_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_site_position` WRITE;
/*!40000 ALTER TABLE `dux_site_position` DISABLE KEYS */;

INSERT INTO `dux_site_position` (`pos_id`, `name`, `sort`)
VALUES
	(1,'全局推荐',0);

/*!40000 ALTER TABLE `dux_site_position` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_site_search
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_search`;

CREATE TABLE `dux_site_search` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `keyword` varchar(20) NOT NULL DEFAULT '',
  `num` int(10) NOT NULL DEFAULT '1',
  `app` varchar(20) NOT NULL DEFAULT '',
  `create_time` int(10) NOT NULL DEFAULT '0',
  `update_time` int(10) NOT NULL DEFAULT '0',
  `has_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`search_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_site_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_site_tags`;

CREATE TABLE `dux_site_tags` (
  `tag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `app` varchar(20) NOT NULL,
  `name` varchar(250) NOT NULL DEFAULT '',
  `quote` int(10) NOT NULL DEFAULT '1',
  `view` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tag_id`),
  KEY `name` (`name`),
  KEY `quote` (`quote`),
  KEY `view` (`view`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_system_file
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_system_file`;

CREATE TABLE `dux_system_file` (
  `file_id` int(10) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) NOT NULL DEFAULT '',
  `original` varchar(250) NOT NULL DEFAULT '',
  `title` varchar(250) NOT NULL DEFAULT '',
  `ext` varchar(20) NOT NULL DEFAULT '',
  `size` int(10) NOT NULL DEFAULT '0',
  `time` int(10) NOT NULL DEFAULT '0',
  `user_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `ext` (`ext`),
  KEY `time` (`time`) USING BTREE,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='上传文件';



# Dump of table dux_system_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_system_info`;

CREATE TABLE `dux_system_info` (
  `info_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) NOT NULL DEFAULT '' COMMENT '键名',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '配置名称',
  `value` text NOT NULL COMMENT '配置值',
  `description` varchar(250) NOT NULL DEFAULT '' COMMENT '配置描述',
  `reserve` tinyint(1) NOT NULL DEFAULT '0' COMMENT '内置',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_system_notice
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_system_notice`;

CREATE TABLE `dux_system_notice` (
  `notice_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(20) NOT NULL DEFAULT '',
  `content` varchar(250) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `time` int(10) NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT 'primary',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_system_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_system_role`;

CREATE TABLE `dux_system_role` (
  `role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '',
  `description` varchar(250) NOT NULL DEFAULT '',
  `purview` text NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_system_role` WRITE;
/*!40000 ALTER TABLE `dux_system_role` DISABLE KEYS */;

INSERT INTO `dux_system_role` (`role_id`, `name`, `description`, `purview`)
VALUES
	(1,'管理员','系统后台管理员','a:117:{i:0;s:21:\"article.Content.index\";i:1;s:19:\"article.Content.add\";i:2;s:20:\"article.Content.edit\";i:3;s:22:\"article.Content.status\";i:4;s:19:\"article.Content.del\";i:5;s:19:\"article.Class.index\";i:6;s:17:\"article.Class.add\";i:7;s:18:\"article.Class.edit\";i:8;s:20:\"article.Class.status\";i:9;s:17:\"article.Class.del\";i:10;s:15:\"page.Page.index\";i:11;s:13:\"page.Page.add\";i:12;s:14:\"page.Page.edit\";i:13;s:16:\"page.Page.status\";i:14;s:13:\"page.Page.del\";i:15;s:17:\"site.Config.index\";i:16;s:15:\"site.Config.tpl\";i:17;s:21:\"site.FormManage.index\";i:18;s:19:\"site.FormManage.add\";i:19;s:20:\"site.FormManage.edit\";i:20;s:19:\"site.FormManage.del\";i:21;s:20:\"site.FormField.index\";i:22;s:18:\"site.FormField.add\";i:23;s:19:\"site.FormField.edit\";i:24;s:18:\"site.FormField.del\";i:25;s:22:\"site.ModelManage.index\";i:26;s:20:\"site.ModelManage.add\";i:27;s:21:\"site.ModelManage.edit\";i:28;s:20:\"site.ModelManage.del\";i:29;s:21:\"site.ModelField.index\";i:30;s:19:\"site.ModelField.add\";i:31;s:20:\"site.ModelField.edit\";i:32;s:19:\"site.ModelField.del\";i:33;s:14:\"site.Nav.index\";i:34;s:12:\"site.Nav.add\";i:35;s:13:\"site.Nav.edit\";i:36;s:15:\"site.Nav.status\";i:37;s:12:\"site.Nav.del\";i:38;s:19:\"site.NavGroup.index\";i:39;s:17:\"site.NavGroup.add\";i:40;s:18:\"site.NavGroup.edit\";i:41;s:20:\"site.NavGroup.status\";i:42;s:17:\"site.NavGroup.del\";i:43;s:19:\"site.Fragment.index\";i:44;s:17:\"site.Fragment.add\";i:45;s:18:\"site.Fragment.edit\";i:46;s:20:\"site.Fragment.status\";i:47;s:17:\"site.Fragment.del\";i:48;s:19:\"site.Position.index\";i:49;s:17:\"site.Position.add\";i:50;s:18:\"site.Position.edit\";i:51;s:20:\"site.Position.status\";i:52;s:17:\"site.Position.del\";i:53;s:17:\"site.Filter.index\";i:54;s:15:\"site.Filter.add\";i:55;s:16:\"site.Filter.edit\";i:56;s:18:\"site.Filter.status\";i:57;s:15:\"site.Filter.del\";i:58;s:14:\"site.Adv.index\";i:59;s:12:\"site.Adv.add\";i:60;s:13:\"site.Adv.edit\";i:61;s:15:\"site.Adv.status\";i:62;s:12:\"site.Adv.del\";i:63;s:22:\"site.AdvPosition.index\";i:64;s:20:\"site.AdvPosition.add\";i:65;s:21:\"site.AdvPosition.edit\";i:66;s:23:\"site.AdvPosition.status\";i:67;s:20:\"site.AdvPosition.del\";i:68;s:20:\"site.AdvRegion.index\";i:69;s:18:\"site.AdvRegion.add\";i:70;s:19:\"site.AdvRegion.edit\";i:71;s:21:\"site.AdvRegion.status\";i:72;s:18:\"site.AdvRegion.del\";i:73;s:17:\"site.Search.index\";i:74;s:15:\"site.Search.add\";i:75;s:16:\"site.Search.edit\";i:76;s:15:\"site.Search.del\";i:77;s:18:\"system.Index.index\";i:78;s:21:\"system.Index.userData\";i:79;s:19:\"system.Notice.index\";i:80;s:17:\"system.Notice.del\";i:81;s:19:\"system.Update.index\";i:82;s:19:\"system.Config.index\";i:83;s:18:\"system.Config.user\";i:84;s:18:\"system.Config.info\";i:85;s:20:\"system.Config.upload\";i:86;s:25:\"system.ConfigManage.index\";i:87;s:23:\"system.ConfigManage.add\";i:88;s:24:\"system.ConfigManage.edit\";i:89;s:26:\"system.ConfigManage.status\";i:90;s:23:\"system.ConfigManage.del\";i:91;s:17:\"system.User.index\";i:92;s:15:\"system.User.add\";i:93;s:16:\"system.User.edit\";i:94;s:18:\"system.User.status\";i:95;s:15:\"system.User.del\";i:96;s:17:\"system.Role.index\";i:97;s:15:\"system.Role.add\";i:98;s:16:\"system.Role.edit\";i:99;s:15:\"system.Role.del\";i:100;s:24:\"system.Application.index\";i:101;s:22:\"system.Application.add\";i:102;s:23:\"system.Application.edit\";i:103;s:22:\"system.Application.del\";i:104;s:16:\"tools.Send.index\";i:105;s:14:\"tools.Send.add\";i:106;s:15:\"tools.Send.info\";i:107;s:20:\"tools.SendConf.index\";i:108;s:22:\"tools.SendConf.setting\";i:109;s:19:\"tools.SendTpl.index\";i:110;s:17:\"tools.SendTpl.add\";i:111;s:18:\"tools.SendTpl.edit\";i:112;s:17:\"tools.SendTpl.del\";i:113;s:23:\"tools.SendDefault.index\";i:114;s:17:\"tools.Label.index\";i:115;s:17:\"tools.Queue.index\";i:116;s:21:\"tools.QueueConf.index\";}');

/*!40000 ALTER TABLE `dux_system_role` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_system_statistics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_system_statistics`;

CREATE TABLE `dux_system_statistics` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` varchar(8) NOT NULL DEFAULT '',
  `web` int(10) NOT NULL DEFAULT '0',
  `api` int(10) NOT NULL DEFAULT '0',
  `mobile` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_system_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_system_user`;

CREATE TABLE `dux_system_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) NOT NULL DEFAULT '0',
  `nickname` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `avatar` varchar(250) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `reg_time` int(10) NOT NULL DEFAULT '0',
  `login_time` int(10) NOT NULL DEFAULT '0',
  `login_ip` varchar(50) NOT NULL DEFAULT '',
  `role_ext` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_system_user` WRITE;
/*!40000 ALTER TABLE `dux_system_user` DISABLE KEYS */;

INSERT INTO `dux_system_user` (`user_id`, `role_id`, `nickname`, `username`, `password`, `avatar`, `status`, `reg_time`, `login_time`, `login_ip`, `role_ext`)
VALUES
	(1,1,'Dux','admin','21232f297a57a5a743894a0e4a801fc3','',1,0,1526018868,'127.0.0.1','');

/*!40000 ALTER TABLE `dux_system_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_tools_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_tools_queue`;

CREATE TABLE `dux_tools_queue` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(20) NOT NULL COMMENT '关联标记',
  `has_id` int(10) NOT NULL DEFAULT '0' COMMENT '关联ID',
  `target` varchar(250) NOT NULL DEFAULT '' COMMENT '模块',
  `action` varchar(20) NOT NULL COMMENT '方法名',
  `layer` varchar(20) NOT NULL DEFAULT '' COMMENT '层',
  `params` text NOT NULL COMMENT '参数',
  `remark` varchar(250) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `start_time` int(10) NOT NULL DEFAULT '0' COMMENT '开始时间',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `run_time` int(10) NOT NULL DEFAULT '0' COMMENT '执行时间',
  `run_num` int(3) NOT NULL DEFAULT '0' COMMENT '运行次数',
  `max_num` int(2) NOT NULL DEFAULT '0' COMMENT '最大次数',
  `message` text NOT NULL COMMENT '返回消息',
  PRIMARY KEY (`queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table dux_tools_queue_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_tools_queue_config`;

CREATE TABLE `dux_tools_queue_config` (
  `config_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT '' COMMENT '类型名',
  `content` text NOT NULL COMMENT '配置内容',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_tools_queue_config` WRITE;
/*!40000 ALTER TABLE `dux_tools_queue_config` DISABLE KEYS */;

INSERT INTO `dux_tools_queue_config` (`config_id`, `name`, `content`)
VALUES
	(1,'every_num','10'),
	(2,'retry_num','3'),
	(3,'del_status','0'),
	(4,'status','1'),
	(5,'lock_time','60');

/*!40000 ALTER TABLE `dux_tools_queue_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_tools_send
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_tools_send`;

CREATE TABLE `dux_tools_send` (
  `send_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `receive` varchar(250) NOT NULL DEFAULT '' COMMENT '接收账号',
  `title` varchar(250) NOT NULL DEFAULT '' COMMENT '发送标题',
  `content` text NOT NULL COMMENT '发送内容',
  `param` text NOT NULL COMMENT '附加参数',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '发送状态',
  `type` varchar(50) NOT NULL DEFAULT '' COMMENT '发送类型',
  `start_time` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `stop_time` int(10) NOT NULL DEFAULT '0' COMMENT '结束时间',
  `remark` varchar(250) NOT NULL DEFAULT '' COMMENT '备注',
  `user_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否会员',
  PRIMARY KEY (`send_id`),
  KEY `type` (`type`),
  KEY `start_time` (`start_time`),
  KEY `stop_time` (`stop_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;



# Dump of table dux_tools_send_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_tools_send_config`;

CREATE TABLE `dux_tools_send_config` (
  `config_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` varchar(250) NOT NULL DEFAULT '' COMMENT '类型名',
  `setting` text NOT NULL COMMENT '配置内容',
  PRIMARY KEY (`config_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;



# Dump of table dux_tools_send_default
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_tools_send_default`;

CREATE TABLE `dux_tools_send_default` (
  `default_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '' COMMENT '种类',
  `type` varchar(50) NOT NULL DEFAULT '' COMMENT '类型',
  `tpl` text NOT NULL COMMENT '基础模板',
  PRIMARY KEY (`default_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `dux_tools_send_default` WRITE;
/*!40000 ALTER TABLE `dux_tools_send_default` DISABLE KEYS */;

INSERT INTO `dux_tools_send_default` (`default_id`, `class`, `type`, `tpl`)
VALUES
	(1,'sms','alsms',''),
	(2,'mail','email','<div style=\"width:700px;margin:0 auto;\">\n<table align=\"center\" border=\"0\" cellspacing=\"0\" style=\"width:700px;\" width=\"700\">\n	<tbody>\n		<tr>\n			<td>\n			<div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;\">\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"39\" style=\"font:12px Tahoma, Arial, 宋体;\" width=\"700\">\n				<tbody>\n					<tr>\n						<td width=\"210\">&nbsp;</td>\n					</tr>\n				</tbody>\n			</table>\n			</div>\n			[内容区域]\n\n			<div style=\"width:700px;margin:0 auto;\">\n			<div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">\n			<p>此为系统邮件，请勿回复<br />\n			请保管好您的邮箱，避免账号被他人盗用</p>\n\n			<p>[网站名称] <span style=\"border-bottom: 1px dashed rgb(204, 204, 204); z-index: 1; position: static;\">[网址]</span></p>\n			</div>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n</div>\n\n<p>&nbsp;</p>\n'),
	(3,'wechat','wechat',''),
	(4,'app','xiaomi',''),
	(5,'mail_tpl','<div style=\"width:700px;margin:0 auto;\">\n<table al',''),
	(6,'site','site','');

/*!40000 ALTER TABLE `dux_tools_send_default` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table dux_tools_send_tpl
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dux_tools_send_tpl`;

CREATE TABLE `dux_tools_send_tpl` (
  `tpl_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL DEFAULT '' COMMENT '模板标题',
  `content` text NOT NULL COMMENT '模板内容',
  `time` int(10) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`tpl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
