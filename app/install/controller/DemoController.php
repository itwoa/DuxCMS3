<?php

namespace app\install\controller;

ignore_user_abort(true);
set_time_limit(1800);


use QL\QueryList;

/**
 * 演示数据
 */
class DemoController extends \dux\kernel\Controller {

    public function classData() {
        return ROOT_PATH . '/app/install/data/class.xml';
    }

    public function contentData() {
        return ROOT_PATH . '/app/install/data/content.xml';
    }

    public function index() {

        $urlBase = 'https://www.huxiu.com';

        //栏目采集
        $classData = QueryList::get($urlBase)->rules([
            'title' => array('.header-column-zx li a', 'text'),
            'link' => array('.header-column-zx li a', 'href')
        ])->query()->getData()->all();

        file_put_contents($this->classData(), $this->xml_encode($classData));

        //标题采集
        $contentUn = [];
        $contentData = [];
        foreach ($classData as $key => $vo) {
            $url = $urlBase . $vo['link'];
            $data = QueryList::get($url)->rules([
                'image' => array('.mod-art .mod-thumb img', 'src'),
                'title' => array('.mod-art .mob-ctt h2 a', 'text'),
                'link' => array('.mod-art .mob-ctt h2 a', 'href')
            ])->query()->getData()->all();

            foreach ($data as $k => $v) {
                if($contentUn[$v['title']]) {
                    unset($data[$k]);
                    continue;
                }
                $data[$k]['class'] = $key;
                $contentUn[$v['title']] = true;
            }
            $contentData = array_merge($contentData, $data);
        }


        //内容采集
        foreach ($contentData as $key => $vo) {
            $url = $urlBase . $vo['link'];
            $data = QueryList::get($url)->rules([
                'time' => array('.article-author .article-time', 'text'),
                'content' => array('.article-content-wrap', 'html', '-.neirong-shouquan, -.neirong-shouquan-public'),
                'keywords' => array("meta[name='keywords']", 'content')
            ])->query()->getData()->all();

            $contentData[$key]['keywords'] = $data[0]['keywords'];
            $contentData[$key]['time'] = $data[0]['time'];
            $contentData[$key]['content'] = html_in($data[0]['content']);
        }
        file_put_contents($this->contentData(), $this->xml_encode($contentData));

        echo '演示数据采集完毕！';
    }

    public function test() {
        $class = json_decode(json_encode(simplexml_load_string(file_get_contents($this->classData()), 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        $content = json_decode(json_encode(simplexml_load_string(file_get_contents($this->contentData()), 'SimpleXMLElement', LIBXML_NOCDATA)), true);


        foreach ($class['item'] as $key => $vo) {
            $_POST = [
                'name' => $vo['title']
            ];
            $classId = target('article/ArticleClass')->saveData('add');
            if(!$classId) {
                echo target('article/ArticleClass')->getError();
                exit;
            }
            $class['item'][$key]['class_id'] = $classId;
        }

        foreach ($content['item'] as $key => $vo) {
            $_POST = [
                'title' => $vo['title'],
                'class_id' => $class['item'][$vo['class']]['class_id'],
                'image' => $vo['image'],
                'keyword' => $vo['keywords'],
                'content' => $vo['content'],
                'create_time' => $vo['time'],
                'status' => 1
            ];
            $status = target('article/Article')->saveData('add');
            if(!$status) {
                echo target('article/Article')->getError();
                exit;
            }
        }

        echo '演示数据导入成功！';
    }

    public function xml_encode($data, $encoding = 'utf-8', $root = 'demo') {
        $xml = '<?xml version="1.0" encoding="' . $encoding . '"?>';
        $xml .= '<' . $root . '>';
        $xml .= $this->data_to_xml($data);
        $xml .= '</' . $root . '>';
        return $xml;
    }

    public function data_to_xml($data) {
        $xml = '';
        foreach ($data as $key => $val) {
            is_numeric($key) && $key = "item id=\"$key\"";
            $xml .= "<$key>";
            $xml .= (is_array($val) || is_object($val)) ? $this->data_to_xml($val) : $val;
            list($key,) = explode(' ', $key);
            $xml .= "</$key>";
        }
        return $xml;
    }

}